from selenium.webdriver.common.by import By


class Sprint_page():

    def __init__(self, driver):
        self.driver = driver

    # Locators
    _create_room_button = "//button[@class='gs-room__button gs-room__button--create']"
    _userbar = "//div[@class='gs-userbar']"
    _logout_button = "//li[@class='gs-dropdown__item gs-dropdown__item--nolink gs-dropdown__item--red gs-dropdown__item-btn-logout']"

    def get_create_room_button(self):
        return self.driver.find_element(By.XPATH, self._create_room_button)

    def get_userbar(self):
        return self.driver.find_element(By.XPATH, self._userbar)

    def get_logout_button(self):
        return self.driver.find_element(By.XPATH, self._logout_button)

    def userbar_expand(self):
        self.get_userbar().click()

    def logout_click(self):
        self.get_logout_button().click()

    def logoff_user(self):
        self.userbar_expand()
        self.logout_click()
