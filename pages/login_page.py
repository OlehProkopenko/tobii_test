from selenium.webdriver.common.by import By


class Login_page():

    def __init__(self, driver):
        self.driver = driver

    # Locators
    _email_field = "//input[@placeholder='yours@example.com']"
    _password_field = "//input[@placeholder='your password']"
    _login_button = "//button[@name='submit']"

    def get_email_field(self):
        return self.driver.find_element(By.XPATH, self._email_field)

    def get_password_field(self):
        return self.driver.find_element(By.XPATH, self._password_field)

    def get_login_button(self):
        return self.driver.find_element(By.XPATH, self._login_button)

    def enter_email(self, email):
        self.get_email_field().send_keys(email)

    def enter_password(self, password):
        self.get_password_field().send_keys(password)

    def click_login_button(self):
        self.get_login_button().click()

    def login(self, email, password):
        self.enter_email(email)
        self.enter_password(password)
        self.click_login_button()
