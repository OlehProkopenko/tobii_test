from selenium import webdriver
from pages.login_page import Login_page
from pages.sprint_page import Sprint_page
import unittest

class Login_test(unittest.TestCase):
    def test_login_and_logoff(self):
        login_url = "https://sprint.tobiipro.com"
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.implicitly_wait(4)
        driver.get(login_url)

        login_page = Login_page(driver)
        sprint_page = Sprint_page(driver)
        login_page.login("sprint.test@tobii.com", "Tobii123!")
        assert sprint_page.get_create_room_button()
        sprint_page.logoff_user()
        assert login_page.get_login_button()
